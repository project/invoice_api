<?php

/**
 * Implements hook_entity_info().
 */
function invoice_api_entity_info() {
  $info['invoice_api'] = array(
    'label' => t('Invoice API'),
    'controller class' => 'InvoiceAPIEntityController',
    'base table' => 'invoice_api',
    'fieldable' => FALSE,
    'entity keys' => array(
      'id' => 'invoice_id',
      'bundle' => 'type',
      'label' => 'invoice_id',
    ),
    'bundle keys' => array(
      'bundle' => 'type',
    ),
    'bundles' => array(
      'invoice_api' => array(
        'label' => t('Invoice', array(), array('context' => 'a drupal commerce invoice')),
      ),
    ),
    'load hook' => 'invoice_api_load',
    'view modes' => array(
      'administrator' => array(
        'label' => t('Administrator'),
        'custom settings' => FALSE,
      ),
      'customer' => array(
        'label' => t('Customer'),
        'custom settings' => FALSE,
      ),
    ),
    'creation callback' => '_invoice_api_create',
    'save callback' => 'invoice_api_save',
    'deletion callback' => 'invoice_api_delete',
    'access callback' => 'commerce_entity_access',
    'access arguments' => array(
      'user key' => 'uid',
      'access tag' => 'invoice_api_access',
    ),
    'token type' => 'invoice-api',
    'permission labels' => array(
      'singular' => t('invoice'),
      'plural' => t('invoices'),
    ),
  );
  
  $info['invoice_api_method'] = array(
    'label' => t('Invopice API method'),
    'controller class' => 'EntityAPIControllerExportable',
    'metadata controller class' => FALSE,
    'entity class' => 'InvoiceApiMethod',
    'base table' => 'invoice_api_method',
    'module' => 'invoice_api',
    'exportable' => TRUE,
    'entity keys' => array(
      'id' => 'id',
      'label' => 'name',
      'name' => 'machine_name',
    ),
  );

  return $info;
}

/**
 * Implements hook_menu().
 */
function invoice_api_menu() {
  $items = array();
  
  $items['admin/commerce/config/invoice_api'] = array(
    'title' => 'Invoice API',
    'description' => 'Configure general invoice API settings.',
    'page callback' => 'invoice_api_settings',
    'access arguments' => array('configure invoice api settings'),
    'file' => 'includes/invoice_api.settings.inc',
  );
  $items['admin/commerce/config/invoice_api/api'] = array(
    'title' => 'Select API',
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'weight' => -10,
  );
  
  $items['admin/commerce/config/invoice_api/add'] = array(
    'title' => 'Add method',
    'description' => 'Create a new invoice api method.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('invoice_api_add_method_form'),
    'access arguments' => array('configure invoice api settings'),
    'file' => 'includes/invoice_api.settings.inc',
    'weight' => -1,
    'type' => MENU_LOCAL_ACTION,
  );
  
  $items['admin/commerce/config/invoice_api/method/%invoice_api_method'] = array(
    'title' => 'View server',
    'title callback' => 'search_api_admin_item_title',
    'title arguments' => array(5),
    'description' => 'View server details.',
    'page callback' => 'invoice_api_method_view',
    'page arguments' => array(5),
    'access arguments' => array('configure invoice api settings'),
    'file' => 'includes/invoice_api.settings.inc',
  );
  $items['admin/commerce/config/invoice_api/method/%invoice_api_method/view'] = array(
    'title' => 'View',
    'weight' => -10,
    'type' => MENU_DEFAULT_LOCAL_TASK,
  );
  $items['admin/commerce/config/invoice_api/method/%invoice_api_method/edit'] = array(
    'title' => 'Edit',
    'description' => 'Edit method details.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('invoice_api_add_method_edit_form', 5),
    'access arguments' => array('configure invoice api settings'),
    'file' => 'search_api.admin.inc',
    'weight' => -1,
    'type' => MENU_LOCAL_TASK,
  );
  
  $items['admin/commerce/invoices'] = array(
    'title' => 'Invoices',
    'description' => 'View invoices.',
    'page callback' => 'invoices_api_ui_invoices',
    'access arguments' => array('configure invoice api settings'),
  );
  $items['admin/commerce/invoices/overview'] = array(
    'title' => 'Invoices',
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'weight' => -10,
  );
  
  $items['admin/commerce/orders/%commerce_order/invoices'] = array(
    'title' => 'Invoice',
    'page callback' => 'invoice_api_invoice_view_by_order',
    'page arguments' => array(3),
    'access callback' => 'invoice_api_access_invoice_by_order',
    'access arguments' => array('view', 3),
    'type' => MENU_LOCAL_TASK,
    'weight' => -5,
    'context' => MENU_CONTEXT_PAGE | MENU_CONTEXT_INLINE,
  );

  return $items;
}

/**
 * Implements hook_permission().
 */
function invoice_api_permission() {
  return commerce_entity_access_permissions('invoice_api') + array(
    'configure invoice api settings' => array(
      'title' => t('Configure invoice api settings'),
      'description' => t('Allows users to configure invoice api settings for the store.'),
      'restrict access' => TRUE,
    ),
  );
}

/**
 * Implements hook_theme().
 */
function invoice_api_theme() {
  $themes['invoice_api_method'] = array(
    'variables' => array(
      'id' => NULL,
      'name' => '',
      'machine_name' => '',
      'description' => NULL,
      'invoice_number' => NULL,
      'invoice_file' => NULL,
      'enabled' => NULL,
      'options' => array(),
      'status' => ENTITY_CUSTOM,
    ),
    'file' => 'includes/invoice_api.settings.inc',
  );

  return $themes;
}

/**
 * Saves an invoice.
 *
 * @param $invoice
 *   The full invoice object to save.
 *
 * @return
 *   The saved invoice object.
 */
function invoice_api_save($invoice) {
  return entity_get_controller('invoice_api')->save($invoice);
}

/**
 * Deletes an invoice by ID.
 *
 * @param $invoice
 *   The ID of the invoice to delete.
 *
 * @return
 *   TRUE on success, FALSE otherwise.
 */
function invoice_api_delete($invoice_id) {
  // return commerce_invoice_delete_multiple(array($invoice_id));
}

/**
 * Deletes multiple invoices by ID.
 *
 * @param $invoice_ids
 *   An array of invoice IDs to delete.
 *
 * @return
 *   TRUE on success, FALSE otherwise.
 */
function invoice_api_delete_multiple($invoice_ids) {
  // return entity_get_controller('invoice_api')->delete($invoice_ids);
}

/**
 * Checks invoice access for various operations.
 *
 * @param $op
 *   The operation being performed. One of 'view', 'update', 'create' or
 *   'delete'.
 * @param $invoice
 *   Optionally an invoice to check access for.
 * @param $account
 *   The user to check for. Leave it to NULL to check for the current user.
 */
function invoice_api_access($op, $invoice = NULL, $account = NULL) {
  return commerce_entity_access($op, $invoice, $account, 'invoice_api');
}

/**
 * Returns an initialized invoice object.
 *
 * @param $uid
 *   The uid of the owner of the invoice.
 * @param $order_id
 *   The ID of the order the invoice belongs to (if available).
 * @param $type
 *   The type of the invoice; defaults to the standard 'invoice' type.
 *
 * @return
 *   An invoice object with all default fields initialized.
 */
function invoice_api_new($uid = 0, $order_id = 0, $type = 'invoice_api') {
  return entity_get_controller('invoice_api')->create(array('uid' => $uid, 'order_id' => $order_id, 'type' => $type));
}

/**
 * Loads an invoice by ID.
 */
function invoice_api_load($invoice_id) {
  $invoices = invoice_api_load_multiple(array($invoice_id), array());
  return $invoices ? reset($invoices) : FALSE;
}

/**
 * Loads multiple invoices by ID or based on a set of matching conditions.
 *
 * @see entity_load()
 *
 * @param $invoice_ids
 *   An array of invoice IDs.
 * @param $conditions
 *   An array of conditions on the {commerce_invoice} table in the form
 *     'field' => $value.
 * @param $reset
 *   Whether to reset the internal invoice loading cache.
 *
 * @return
 *   An array of invoice objects indexed by invoice_id.
 */
function invoice_api_load_multiple($invoice_ids = array(), $conditions = array(), $reset = FALSE) {
  return entity_load('invoice_api', $invoice_ids, $conditions, $reset);
}

/**
 * Returns the name of the specified invoice type or all names keyed by type if no
 *   type is specified.
 *
 * @param $type
 *   The invoice type whose name should be returned; corresponds to the bundle key
 *     in the invoice entity definition.
 *
 * @return
 *   Either the specified name, defaulting to the type itself if the name is not
 *   found, or an array of all names keyed by type if no type is passed in.
 */
function invoice_api_type_get_name($type = NULL) {
  $names = array();

  $entity = entity_get_info('invoice_api');

  foreach ($entity['bundles'] as $key => $value) {
    $names[$key] = $value['label'];
  }

  if (empty($type)) {
    return $names;
  }

  if (empty($names[$type])) {
    return check_plain($type);
  }
  else {
    return $names[$type];
  }
}

/**
 * Implements hook_views_api().
 */
function invoice_api_views_api() {
  return array(
    'api' => 3,
    'path' => drupal_get_path('module', 'invoice_api') . '/includes/views',
  );
}

function invoices_api_ui_invoices() {
  // TODO
  return "TEST";
}

/**
 * Checks invoice access based on order id.
 *
 * @param $op
 *   The operation being performed. One of 'view', 'update', 'create' or
 *   'delete'.
 * @param $order
 *   Optionally an order to check access for.
 * @param $account
 *   The user to check for. Leave it to NULL to check for the current user.
 */
function invoice_api_access_invoice_by_order($op, $order = NULL, $account = NULL) {
  $invoice = invoice_api_load_by_order_id($order->order_id);
  
  if ($invoice) {
    return invoice_api_access($op, $invoice, $account, 'invoice_api');
  }
  else {
    return FALSE;
  }
}

/**
 * Generate an array for rendering the given invoice, based on an order.
 *
 * @param $order
 *   A fully loaded order object.
 * @param $view_mode
 *   The view mode for displaying the invoice, 'administrator' or 'customer'.
 *
 * @return
 *   An array as expected by drupal_render().
 */
function invoice_api_invoice_view_by_order($order, $view_mode = 'administrator', $breadcrumb = TRUE) {
  $invoice = invoice_api_load_by_order_id($order->order_id);
  
  if ($invoice) {
    return invoice_api_invoice_view($invoice, $view_mode, $breadcrumb);
  }
  else {
    drupal_set_message(t('The invoice for this order has not been generated yet'), 'warning');
    return '';
  }
}

/**
 * Loads an invoice by order ID
 */
function invoice_api_load_by_order_id($order_id) {
  $query = new EntityFieldQuery();
  
  $result = $query
    ->entityCondition('entity_type', 'invoice_api')
    ->propertyCondition('order_id', $order_id)
    ->execute();
  return $result ? invoice_api_load(reset(array_keys($result['invoice_api']))) : FALSE;
}

/**
 * Generate an array for rendering the given invoice.
 *
 * @param $invoice
 *   A fully loaded invoice object.
 * @param $view_mode
 *   The view mode for displaying the invoice, 'administrator' or 'customer'.
 *
 * @return
 *   An array as expected by drupal_render().
 */
function invoice_api_invoice_view($invoice, $view_mode = 'administrator', $breadcrumb = TRUE) {
  return entity_view('invoice_api', array($invoice->invoice_id => $invoice), $view_mode, NULL, TRUE);
}

/**
 * Load the invoice method with the specified id.
 *
 * @param $id
 *   The search server's id.
 * @param $reset
 *   Whether to reset the internal cache.
 *
 * @return InvoiceApiMethod
 *   An object representing the server with the specified id.
 */
function invoice_api_method_load($id, $reset = FALSE) {
  $ret = invoice_api_method_load_multiple(array($id), array(), $reset);
  return $ret ? reset($ret) : FALSE;
}

/**
 * Load multiple methods at once, determined by IDs or machine names, or by
 * other conditions.
 *
 * @see entity_load()
 *
 * @param array|false $ids
 *   An array of server IDs or machine names, or FALSE to load all servers.
 * @param array $conditions
 *   An array of conditions on the {invoice_api_method} table in the form
 *   'field' => $value.
 * @param bool $reset
 *   Whether to reset the internal entity_load cache.
 *
 * @return array
 *   An array of server objects keyed by machine name.
 */
function invoice_api_method_load_multiple($ids = array(), $conditions = array(), $reset = FALSE) {
  $servers = entity_load('invoice_api_method', $ids, $conditions, $reset);
  return entity_key_array_by_property($servers, 'machine_name');
}