<?php

/**
 * Export Drupal Commerce invoices to Views.
 */

/**
 * Implements hook_views_data()
 */
function invoice_api_views_data() {
  $data = array();

  $data['invoice_api']['table']['group']  = t('Invoice API');

  $data['invoice_api']['table']['base'] = array(
    'field' => 'invoice_number',
    'title' => t('Invoice API'),
    'help' => t('Invoice generated for an order.'),
  );
  
  $data['invoice_api']['table']['join'] = array(
    'invoice_api_files' => array(
      'left_field' => 'invoice_id',
      'field' => 'invoice_id'
    ),
  );
    
  // Expose the invoice ID.
  $data['invoice_api']['invoice_id'] = array(
    'title' => t('Invoice ID', array(), array('context' => 'a drupal commerce invoice')),
    'help' => t('The unique internal identifier of the invoice.'),
    'field' => array(
      'handler' => 'invoice_api_handler_field_invoice',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
    ),
  );
  
  // Expose the invoice number.
  $data['invoice_api']['invoice_number'] = array(
    'title' => t('Invoice number', array(), array('context' => 'a drupal commerce invoice')),
    'help' => t('The unique invoice number.'),
    'field' => array(
      'handler' => 'invoice_api_handler_field_invoice',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );
  
  // Expose the order id
  $data['invoice_api']['order_id'] = array(
    'title' => t('Order id'),
    'help' => t('The order id associated to the invoice.'),
    'relationship' => array(
      'title' => t('Order'),
      'help' => t('Relate this invoice to its order'),
      'handler' => 'views_handler_relationship',
      'base' => 'commerce_order',
      'base field' => 'order_id',
      'field' => 'order_id',
      'label' => t('Order associated to the invoice'),
    ),
  );
  
  /*
  // Relationship to invoice_api_files
  $data['invoice_api']['invoice_api_files'] = array(
    'title' => t('Invoice files'),
    'help' => t('The files associated with this invoice.'),
    'relationship' => array(
      'title' => t('Invoice Files'),
      'help' => t('Relate this invoice to its order'),
      'handler' => 'views_handler_relationship',
      'base' => 'commerce_order',
      'base field' => 'order_id',
      'field' => 'order_id',
      'label' => t('Order associated to the invoice'),
    ),
  );
  */
    
  // Expose the owner uid.
  $data['invoice_api']['uid'] = array(
    'title' => t('Uid'),
    'help' => t("The owner's user ID."),
    'field' => array(
      'handler' => 'views_handler_field_user',
      'click sortable' => TRUE,
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_user_uid',
      'name field' => 'name', // display this field in the summary
    ),
    'filter' => array(
      'title' => t('Name'),
      'handler' => 'views_handler_filter_user_name',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'relationship' => array(
      'title' => t('Owner'),
      'help' => t("Relate this invoice to its owner's user account"),
      'handler' => 'views_handler_relationship',
      'base' => 'users',
      'base field' => 'uid',
      'field' => 'uid',
      'label' => t('Invoice owner'),
    ),
  );
  
  // Expose the created timestamp.
  $data['invoice_api']['created'] = array(
    'title' => t('Created date'),
    'help' => t('The date the invoice was created.'),
    'field' => array(
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_date',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_date',
    ),
  );
  
  
  $data['invoice_api_files']['table']['group']  = t('Invoice API Files');

  $data['invoice_api_files']['table']['base'] = array(
    'field' => 'invoice_number',
    'title' => t('Invoice API Files'),
    'help' => t('Invoice generated for an order.'),
  );
  
  $data['invoice_api_files']['table']['join'] = array(
    'invoice_api' => array(
      'left_field' => 'invoice_id',
      'field' => 'invoice_id'
    ),
  );
  
  $data['invoice_api_files']['invoice_id'] = array(
    'title' => t('Invoice ID'),
    'help' => t("The Invoice ID this file belongs to."),
    'field' => array(
      'handler' => 'invoice_api_handler_field_invoice',
      'click sortable' => TRUE,
    ),
    'relationship' => array(
      'title' => t('Invoice'),
      'help' => t("Relate this file to its invoice."),
      'handler' => 'views_handler_relationship',
      'base' => 'invoice_api',
      'base field' => 'invoice_id',
      'field' => 'invoice_id',
      'label' => t('Invoice'),
    ),
  );
  
  $data['invoice_api_files']['fid'] = array(
    'title' => t('File ID'),
    'help' => t("The fid of the file."),
    'relationship' => array(
      'title' => t('File'),
      'help' => t("Relate to the file."),
      'handler' => 'views_handler_relationship',
      'base' => 'file_managed',
      'base field' => 'fid',
      'field' => 'fid',
      'label' => t('Invoice'),
    ),
  );
  
  return $data;
}

/**
 * Implements hook_views_data_alter()
 */
function invoice_api_views_data_alter(&$data) {
  $data['invoice_api']['invoice_id'] = array(
    'title' => t('Invoice id'),
    'help' => t('The invoice id associated with this order.'),
    'relationship' => array(
      'title' => t('Invoice'),
      'help' => t("Relate this order to its invoice"),
      'handler' => 'views_handler_relationship',
      'base' => 'invoice_api',
      'base field' => 'order_id',
      'relationship field' => 'order_id',
      'field' => 'order_id',
      'label' => t('Invoice associated to the order'),
    ),
  );
}