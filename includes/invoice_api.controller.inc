<?php

/**
 * @file
 * The controller for the invoice entity containing the CRUD operations.
 */

/**
 * The controller class for invoices contains methods for the order CRUD
 * operations. The load method is inherited from the default controller.
 */
class InvoiceApiEntityController extends DrupalCommerceEntityController {

  /**
   * Create a default invoice.
   *
   * @param array $values
   *   An array of values to set, keyed by property name
   *
   * @return
   *   An invoice object with all default fields initialized.
   */
  public function create(array $values = array()) {
    return (object) ($values + array(
      'invoice_id' => '',
      'order_id' => 0,
      'uid' => '',
      'files' => array(
        array('fid' => ''),
      ),
      'created' => '',
      'changed' => '',
      'type' => 'invoice_api',
    ));
  }

  /**
   * Saves an invoice.
   *
   * When saving an invoice, the function will automatically create an invoice number
   * based 
   *
   * @param $invoice
   *   The full invoice object to save.
   * @param $transaction
   *   An optional transaction object.
   *
   * @return
   *   The saved invoice object.
   */
  public function save($invoice, DatabaseTransaction $transaction = NULL) {
    try {
      $invoice->changed = REQUEST_TIME;
      
      // Inserting new invoice
      if (empty($invoice->invoice_id)) {
        $invoice->created = REQUEST_TIME;
        dpm($invoice);
        /*
        
        $infos = module_invoke_all('invoice_api_info');
        $number_method = variable_get('invoice_api_number', '');
        $invoice_method = variable_get('invoice_api_invoice', '');
        
        if(isset($infos[$invoice_method]) && is_array($infos[$invoice_method])) {
          $method = $infos[$invoice_method];
          if(isset($method['number']) && $method['number'] == TRUE) {
            $invoice_file = $this->generate_invoice_file($method['file callback']);
            $invoice->invoice_number = $invoice_file->invoice_number;
            $invoice->files = $invoice_file->files;
          } else {
            $invoice->invoice_number = $this->generate_invoice_number();
            $invoice_file = $this->generate_invoice_file($method['file callback']);
            $invoice->files = $invoice_file->files;
          }
        } elseif(is_array($infos[$number_method])) {
          $method = $infos[$number_method];
          if(isset($method['number callback'])) {
            $invoice->invoice_number = $this->generate_invoice_number($method['number callback']);
          }
        }
        */

        $this->invoke('presave', $invoice);
        // $invoice = $this->_save($invoice, $transaction);
        $this->invoke('insert', $invoice);
      }
      else {
        $this->invoke('presave', $invoice);
        // $invoice = $this->_save($invoice, $transaction);
        $this->invoke('insert', $invoice);
      }
      
      return $invoice;
    }
    catch (Exception $e) {
      throw $e;
    }
  }

  /**
   * Saves an invoice (helper function)
   *
   * @param $invoice
   *   The full invoice object to save.
   * @param $transaction
   *   An optional transaction object.
   *
   * @return
   *   The saved invoice object.
   */
  private function _save($invoice, DatabaseTransaction $transaction = NULL) {
    $transaction = isset($transaction) ? $transaction : db_transaction();
    try {
      if (empty($invoice->invoice_id)) {
        // Save the new invoice
        drupal_write_record('invoice_api', $invoice);
        field_attach_insert('invoice_api', $invoice);
      }
      else {
        drupal_write_record('invoice_api', $invoice, 'invoice_id');
        field_attach_update('invoice_api', $invoice);
      }
      // Ignore slave server temporarily to give time for the
      // saved invoice to be propagated to the slave.
      db_ignore_slave();
      
      $files = array();
      foreach($invoice->files as $key => $file) {
        $files[] = array(
          'invoice_id' => $invoice->invoice_id,
          'fid' => $file->fid,
        );
      }
      
      foreach($files as $key => $file) {
        drupal_write_record('invoice_api_files', $file);
        $files[$key] = $file;
      }
      
      $invoice->files = $files;
      
      return $invoice;
    }
    catch (Exception $e) {
      $transaction->rollback();
      watchdog_exception('invoice_api', $e);
      throw $e;
    }
  }
  

  /**
   * Generates invoice id
   * 
   * @return
   *   The generated invoice id
   */
  protected function generate_invoice_number($callback) {
    return call_user_func_array($callback, array());
  }
  
  /**
   * Attach invoice files
   * 
   * @return
   *   The generated invoice id
   */
  protected function generate_invoice_file($callback) {
    return call_user_func_array($callback, array());
  }
  
  /**
   * Deletes multiple invoices by ID.
   *
   * @param $invoice_ids
   *   An array of invoice IDs to delete.
   * @param $transaction
   *  An optional transaction object.
   *
   * @return
   *   TRUE on success, FALSE otherwise.
   */
  public function delete($invoice_ids, DatabaseTransaction $transaction = NULL) {
    if (!empty($invoice_ids)) {
      $invoices = $this->load($invoice_ids, array());

      $transaction = isset($transaction) ? $transaction : db_transaction();

      try {
        db_delete('invoice_api')
          ->condition('invoice_id', $invoice_ids, 'IN')
          ->execute();

        // Reset the cache as soon as the changes have been applied.
        $this->resetCache($invoice_ids);
 
        foreach ($invoices as $id => $invoice) {
          $this->invoke('delete', $invoice);
        }

        // Ignore slave server temporarily to give time for the
        // saved invoice to be propagated to the slave.
        db_ignore_slave();
      }
      catch (Exception $e) {
        $transaction->rollback();
        watchdog_exception('invoice_api', $e);
        throw $e;
      }

      // Clear the page and block and line_item_load_multiple caches.
      cache_clear_all();
      $this->resetCache();
    }

    return TRUE;
  }
  
  /**
   * Builds a structured array representing the entity's content.
   *
   * The content built for the entity will vary depending on the $view_mode
   * parameter.
   *
   * @param $entity
   *   An entity object.
   * @param $view_mode
   *   View mode, e.g. 'administrator'
   * @param $langcode
   *   (optional) A language code to use for rendering. Defaults to the global
   *   content language of the current request.
   * @return
   *   The renderable array.
   */
  public function buildContent($invoice, $view_mode = 'administrator', $langcode = NULL, $content = array()) {
    // Load the order this invoice is attached to.
    $order = commerce_order_load($invoice->order_id);
    
    $content['invoice_number'] = array(
      '#type' => 'item',
      '#title' => t('Invoice number'),
      '#markup' => $invoice->invoice_number,
    );
    
    $content['invoice_file'] = array(
      '#type' => 'item',
      '#title' => t('Invoice'),
      '#markup' => $invoice->invoice_number,
    );
    
    $content['created'] = array(
      '#type' => 'item',
      '#title' => t('Date'),
      '#markup' => format_date($invoice->created, 'short')
    );
    
    $content['commerce_order'] = entity_build_content('commerce_order', $order, $view_mode, $langcode);

    return parent::buildContent($invoice, $view_mode, $langcode, $content);
  }
}