<?php

function invoice_api_settings() {
  $methods = invoice_api_method_load_multiple(FALSE);
  $rows = array();
  foreach($methods as $method) {
    $row = array(
      'status' => $method->status,
      'name' => l($method->name, 'admin/commerce/config/invoice_api/method/' . $method->machine_name . '/view'),
      'invoice_number' => $method->invoice_number,
      'invoice_file' => $method->invoice_file,
      'operations' => l(t('edit'), 'admin/commerce/config/invoice_api/method/' . $method->machine_name . '/edit')
    );
    $rows[] = $row;
  }
  dpm($methods);
  $variables = array(
    'header' => array(
      'status' => t('Status'),
      'name' => t('Name'),
      'invoice_number' => t('Invoice Nr'),
      'invoice_file' => t('Invoice file'),
      'operations' => t('Operations')
    ),
    'rows' => $rows,
    'attributes' => array(),
    'caption' => '',
    'colgroups' => array(),
    'sticky' => '',
    'empty'=> t('No method available.')
  );
  
  $output = theme_table($variables);
  
  return $output;
}

function invoice_api_add_method_form() {  
  $form = array();
  
  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Name'),
    '#description' => t('Enter the displayed name for the new server.'),
    '#required' => TRUE,
  );
  
  $form['machine_name'] = array(
    '#type' => 'machine_name',
    '#maxlength' => 50,
    '#machine_name' => array(
      'exists' => 'invoice_api_method_load',
    ),
  );
  
  $form['enabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enabled'),
    '#description' => t('Select if the new method will be enabled after creation.'),
    '#default_value' => TRUE,
  );
  
  $form['description'] = array(
    '#type' => 'textarea',
    '#title' => t('Method description'),
    '#description' => t('Enter a description for the new method.'),
  );
  
  $form['invoice_number'] = array(
    '#type' => 'select',
    '#title' => t('Invoice number'),
    '#description' => t('Select the source for the invoice number.'),
    '#options' => _invoice_api_get_method_options($type = 'number'),
  );
  
  $form['invoice_file'] = array(
    '#type' => 'select',
    '#title' => t('Invoice file'),
    '#description' => t('Select the source for the invoice file.'),
    '#options' => _invoice_api_get_method_options($type = 'invoice'),
  );
  
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Create method'),
  );
  
  return $form;
}

/**
 * Form submit callback for adding a method.
 */
function invoice_api_add_method_form_submit(array $form, array &$form_state) {
  dpm($form_state);
  $values = $form_state['values'];
  $options = isset($values['options']['form']) ? $values['options']['form'] : array();
  unset($values['options']);
  $method = entity_create('invoice_api_method', $values);
  $method->save();
}

function _invoice_api_get_method_options($type = 'number') {
  $infos = module_invoke_all('invoice_api_info');
  $options = array();
  switch($type) {
    case 'number':
      foreach($infos as $key => $info) {
        if($info['number'] == TRUE) {
          $options[$key] = $info['title'];
        }
      }
      break;
    
    case 'invoice':
      foreach($infos as $key => $info) {
        if($info['invoice'] == TRUE) {
          $options[$key] = $info['title'];
        }
      }
      break;
  }
  return $options;
}

/**
 * Displays a method's details.
 *
 * @param InvoiceApiMethod $method
 *   The method to display.
 */
function invoice_api_method_view(InvoiceApiMethod $method) {
  dpm($method);
  drupal_set_title($method->name);
  
  return array(
    '#theme' => 'invoice_api_method',
    '#name' => $method->name,
    '#machine_name' => $method->machine_name,
    '#description' => $method->description,
    '#invoice_number' => $method->invoice_number,
    '#invoice_file' => $method->invoice_file,
    '#enabled' => $method->enabled,
    '#status' => $method->status,
  );
   
}

/**
 * Theme function for displaying a method.
 *
 */
function theme_invoice_api_method(array $variables) {
  extract($variables);
  $output = '';

  $output .= '<h3>' . check_plain($name) . '</h3>' . "\n";

  $output .= '<dl>' . "\n";

  $output .= '<dt>' . t('Status') . '</dt>' . "\n";
  $output .= '<dd>';
  if ($enabled) {
    $output .= t('enabled (!disable_link)', array('!disable_link' => l(t('disable'), 'admin/config/search/invoice_api/method/' . $machine_name . '/disable')));
  }
  else {
    $output .= t('disabled (!enable_link)', array('!enable_link' => l(t('enable'), 'admin/config/search/invoice_api/method/' . $machine_name . '/enable', array('query' => array('token' => drupal_get_token($machine_name))))));
  }
  $output .= '</dd>' . "\n";

  $output .= '<dt>' . t('Machine name') . '</dt>' . "\n";
  $output .= '<dd>' . check_plain($machine_name) . '</dd>' . "\n";

  if (!empty($description)) {
    $output .= '<dt>' . t('Description') . '</dt>' . "\n";
    $output .= '<dd>' . nl2br(check_plain($description)) . '</dd>' . "\n";
  }

  if (!empty($invoice_number)) {
    $output .= '<dt>' . t('Invoice number') . '</dt>' . "\n";
    $output .= '<dd>' . check_plain($invoice_number) . '</dd>' . "\n";
  }
  
  if (!empty($invoice_file)) {
    $output .= '<dt>' . t('Invoice file') . '</dt>' . "\n";
    $output .= '<dd>' . check_plain($invoice_file) . '</dd>' . "\n";
  }


  if (!empty($options)) {
    $output .= '<dt>' . t('Service options') . '</dt>' . "\n";
    $output .= '<dd>' . "\n";
    $output .= render($options);
    $output .= '</dd>' . "\n";
  }

  $output .= '<dt>' . t('Configuration status') . '</dt>' . "\n";
  $output .= '<dd>' . "\n";
  $output .= theme('entity_status', array('status' => $status));
  $output .= '</dd>' . "\n";

  $output .= '</dl>';

  return $output;
}