<?php

/**
 * @file
 * Rules integration for invoices.
 *
 * @addtogroup rules
 * @{
 */

/**
 * Implements hook_rules_condition_info()
 */
function invoice_api_rules_condition_info() {
  $items = array();
  $items['invoice_api_exists'] = array(
    'label' => t('Invoice exists for Order'),
    'parameter' => array(
      'commerce_order' => array(
        'type' => 'commerce_order',
        'label' => t('Order to check'),
      ),
    ),
    'group' => t('Commerce Invoice'),
    'callbacks' => array(
      'execute' => 'invoice_api_rules_condition_exists'
    )
  );
  return $items;
}

function invoice_api_rules_condition_exists($order) {
  $order_id = $order->order_id;
  $query = new EntityFieldQuery();
  $results = $query
    ->entityCondition('entity_type', 'invoice_api')
    ->propertyCondition('order_id', $order_id, '=')
    ->execute();
  if(isset($results['invoice_api'])) {
    return TRUE;
  }
  else {
    return FALSE;
  }
}
 
/**
 * Implements hook_rules_action_info().
 */
function invoice_api_rules_action_info() {
  $actions = array();
  
  $methods = invoice_api_method_load_multiple(FALSE);
  
  foreach($methods as $method) {
    $actions[$method->machine_name] = array(
      'label' => t('Perform invoice method:') . ' ' .  t($method->name),
      'parameter' => array(
        'commerce_order' => array(
          'type' => 'commerce_order',
          'label' => t('Order to update'),
        ),
        'invoice_api_method' => array(
          'type' => 'invoice_api_settings',
          'restriction' => 'input',
          'label' => t('Invoice API method'),
          'method' => $method->machine_name,
        ),
      ),
      'group' => t('Invoice API'),
      'callbacks' => array(
        'execute' => 'invoice_api_rules_perform_method',
      ),
    );
  }
 
  return $actions;
}

/**
 * Implements hook_rules_data_info().
 */
function invoice_api_rules_data_info() {
  $data['invoice_api_settings'] = array(
    'label' => t('Payment settings'),
    'ui class' => 'RulesDataUIInvoiceApiSettings',
  );
  return $data;
}


/**
 * Adds a payment method settings form to the enabling action.
 */
class RulesDataUIInvoiceApiSettings extends RulesDataUI implements RulesDataDirectInputFormInterface {
  public static function getDefaultMode() {
    return 'input';
  }

  public static function inputForm($name, $info, $settings, RulesPlugin $element) {
    $form[$name]['method'] = array('#type' => 'value', '#value' => $info['method']);
    return $form;
  }

  public static function render($value) {
    return array();
  }
}

function invoice_api_rules_perform_method($order, $method) {
  $invoice = invoice_api_new($order->uid, $order->order_id);
  invoice_api_save($invoice);
  
  /*
  $method = invoice_api_method_load($method['method']);
  $infos = module_invoke_all('invoice_api_info');
  
  if(isset($method->invoice_number)) {
    if(isset($infos[$method->invoice_number])) {
      $invoice_number_method = $infos[$method->invoice_number];
      
      dpm($invoice_number_method);
    }
  }
  
  dpm($infos);
  dpm($method);
  */
}
